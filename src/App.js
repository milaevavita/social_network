/* eslint-disable react/jsx-no-undef */
import React from "react";

import "./App.sass";
import "./normalize.css";
import Header from "./Components/Header/Header";
import About from "./Components/About/About";
import Profile from "./Components/Profile/Profile";
import Dialogs from "./Components/Dialogs/Dialogs";
import News from "./Components/News/News";
import Music from "./Components/Music/Music";
import Settings from "./Components/Settings/Settings";
import {BrowserRouter, Route} from "react-router-dom";




function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="container">
          <div className="app-wrapper">
            <Header />
            <About />
            <div className="content">
              <Route path='/profile' component={Profile} />
              <Route path='/dialogs' component={Dialogs} />
              <Route path='/news' component={News} />              
              <Route path='/music' component={Music} />              
              <Route path='/settings' component={Settings} />
            </div>
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
