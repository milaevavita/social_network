import React from "react";

import "./Dialogs.sass";

const Dialogs = () => {
  return (
    <div className="dialogs">
      <div className="dialogs-items">
        <h4 className="dialog">Dimych</h4>
        <h4 className="dialog">Andrey</h4>
        <h4 className="dialog">Sveta</h4>
        <h4 className="dialog">Sasha</h4>
        <h4 className="dialog">Viktor</h4>
        <h4 className="dialog">Valera</h4>
      </div>
      <div className="messages">
        <div className="message">Hi</div>
        <div className="message">How is your cactus?</div>
        <div className="message">Yo </div>
      </div>
    </div>
  );
};
export default Dialogs;
