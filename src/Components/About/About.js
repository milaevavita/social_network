import React from "react";

import "./About.sass";
import {NavLink} from "react-router-dom";

const About = () => {
  return (
    <nav className="nav">
      <div className="nav-menu">
        <NavLink to="/profile" className="nav-item " activeClassName="active"> 
          Profile
        </NavLink>
        <NavLink to="/dialogs" className="nav-item" activeClassName="active">
          Messages
        </NavLink>
        <NavLink to="/news" className="nav-item" activeClassName="active">
          News
        </NavLink>
        <NavLink to="/music" className="nav-item" activeClassName="active">
          Music
        </NavLink>
        <NavLink to="/settings" className="nav-item" activeClassName="active">Settings</NavLink>
      </div>
    </nav>
  );
};

export default About;
