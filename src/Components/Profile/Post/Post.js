import React from "react";

import "./Post.sass";
import MyPost from "./MyPost/MyPost";

const Post = () => {
  return (
    <div className="posts">
      <div className="posts-textarea">
        <textarea />
        <button>Add post</button>
      </div>
      <MyPost message="Hi? how are you?" count="15" />
      <MyPost message="It's my first post" count="20" />
    </div>
  );
};

export default Post;
