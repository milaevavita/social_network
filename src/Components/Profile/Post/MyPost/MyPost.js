import React from "react";

import "./MyPost.sass";
import ava from "./it.jpg";

const MyPost = props => {
  return (
    <div className="post-t">
      <div className="post-m">
        <img src={ava} alt="" className="post-item" />
        <p>{props.message}</p>
      </div>
      <span>like {props.count}</span>
    </div>
  );
};

export default MyPost;
