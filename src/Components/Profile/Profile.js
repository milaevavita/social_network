import React from "react";

import "./Profile.sass";
import bg from "./bg1.jpg";
import Post from "./Post/Post";

const Profile = () => {
  return (
    <div >
      <img src={bg} alt="cactus" className="bg" />    
      <Post />  
    </div>
  );
};

export default Profile;
