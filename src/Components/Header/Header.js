import React from "react";

import "./Header.sass";
import logo from "./logo.png";

const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <img src={logo} alt="cactus logo" className="header-logo" />
      </div>
    </header>
  );
};

export default Header;
